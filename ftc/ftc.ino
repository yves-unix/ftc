// SDPX-License-Idenfitier: BSD-3-Clause

#include <newhaven.h>
#include <ui-display.hh>
#include <var8b.hh>
#include <Wire.h>

// Buttons/Inputs
#define BUTTON_IS_PRESSED                       (HIGH)
#define BUTTON_IS_RELEASED                       (LOW)
#define _BUTTONS_PINS_MASK                      (0x1F)
#define BUTTONS_PINS       (PINB & _BUTTONS_PINS_MASK)  

// inputs on Port B --> PINB
#define BUTTON_PLUS_PIN   8
#define BUTTON_MINUS_PIN  9
#define BUTTON_LEFT_PIN  10
#define BUTTON_RIGHT_PIN 11
#define BUTTON_START_PIN 12

// Matching PINS to BUTTONS in PINB
#define BUTTONS_OFF      (0b00000)
#define BUTTON_PLUS_BIT  (0b00001)
#define BUTTON_MINUS_BIT (0b00010)
#define BUTTON_LEFT_BIT  (0b00100)
#define BUTTON_RIGHT_BIT (0b01000)
#define BUTTON_START_BIT (0b10000)

// Actions from button BITS definitions
#define ACTION_PLUS      (BUTTON_PLUS_BIT)
#define ACTION_MINUS     (BUTTON_MINUS_BIT)
#define ACTION_LEFT      (BUTTON_LEFT_BIT)
#define ACTION_RIGHT     (BUTTON_RIGHT_BIT)
#define ACTION_START     (BUTTON_START_BIT)
#define ACTION_STOP      (BUTTON_STOP_BIT)
#define ACTION_RESET     (BUTTON_PLUS_BIT | BUTTON_MINUS_BIT)

//outputs
#define TRIGGER_PIN       4

// infrastructure
//#define DEBUG                           // comment out if not needed

#ifdef DEBUG
  #define DEBUG_S(arg, ...)     Serial.println(arg, ##__VA_ARGS__)
  #define DEBUG_INIT(baud_rate) Serial.begin(baud_rate)
#else
  #define DEBUG_S(arg, ...)
  #define DEBUG_INIT(baud_rate)
#endif

// state machine
enum tState { sINPUT, sRUN };
static tState ui_state = sINPUT;

// ui
static unsigned char prev_buttons_state = LOW;
static ui_display *display;
var8b flash_pops(1, 32, 1); //FP_MIN, FP_MAX, 1, 1);
var8b pop_period(1, 16, 250); //PP_MIN, PP_MAX, 1, 250);  // init val = 1, mult = 250 ms

void config_pins() {
  
  pinMode(BUTTON_PLUS_PIN,  INPUT);
  pinMode(BUTTON_MINUS_PIN, INPUT);
  pinMode(BUTTON_LEFT_PIN,  INPUT);
  pinMode(BUTTON_RIGHT_PIN, INPUT);
  pinMode(BUTTON_START_PIN, INPUT);

  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW);
 
  return;
}

void config_display() {

    display = new ui_display();
    
    display->assign_var(ui_display::disp_mode1, ui_display::disp_left,  "pops",      &flash_pops);
    display->assign_var(ui_display::disp_mode1, ui_display::disp_right, "period", &pop_period);

    display->set_mode(ui_display::disp_mode1);
    display->set_focus(ui_display::disp_left);

    return;
}


tState process_inputs() {

 tState next_state = sINPUT;
 unsigned char buttons_state = BUTTONS_PINS;   
   
  if ( buttons_state == prev_buttons_state ) { 
      
      return next_state;
  } else {

      prev_buttons_state = buttons_state;
     
      if(buttons_state == BUTTONS_OFF)
          return next_state;
          
      DEBUG_S(buttons_state, BIN);
      switch(buttons_state) {

           case ACTION_PLUS:
                DEBUG_S("Plus");
                display->inc_var_in_focus();
                break;
           case ACTION_MINUS:
                DEBUG_S("Minus");
                display->dec_var_in_focus();
                break;
           case ACTION_LEFT:
                DEBUG_S("Left");
                display->set_focus(ui_display::disp_left);
                break;
           case ACTION_RIGHT:
                DEBUG_S("Right");
                display->set_focus(ui_display::disp_right);
                break;          
           case ACTION_START:
                DEBUG_S("Run");
                next_state = sRUN;
                break;        
           default:
               break;;
      }

    }  

    return next_state;
}

void pop_flash() {

  digitalWrite(TRIGGER_PIN, HIGH);
  delay(1);
  digitalWrite(TRIGGER_PIN, LOW);
  return;
}

tState run() {


    tState next_state = sINPUT;
    unsigned long time0, time1;
    var8b flash_pops_run = flash_pops;   // need = operator
    
    display->assign_var(ui_display::disp_mode2, ui_display::disp_left, "pops", &flash_pops_run);
    display->set_mode(ui_display::disp_mode2);  // render
    display->set_focus(ui_display::disp_left);
    for(unsigned char i = 0; i < flash_pops.get_value(); i++) {
        
        time0=millis();
        pop_flash();  // 1ms pulse
        time1=abs(millis()-time0);            // for very long system runs
        delay(pop_period.get_mvalue()-time1);  // might be dangerous for small period values!
        display->dec_var_in_focus();
    }
    display->set_mode(ui_display::disp_mode1); 
    DEBUG_S("done run");
    return next_state;  
}

void setup() {
  // put your setup code here, to run once:
  DEBUG_INIT(9600);
  config_pins();
  config_display();
  ui_state = sINPUT;
}

void loop() {

    switch(ui_state) {
      case sINPUT:
          ui_state = process_inputs();
          break;

      case sRUN:
          ui_state = run();
          break;
      
      default:
          DEBUG_S("What the? unexpected state! %d");
          DEBUG_S((unsigned char)ui_state);
          ui_state = sINPUT;
          break;
    }
 
}
